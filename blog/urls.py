import rest_framework_simplejwt.views as simplejwt_views
from django.contrib import admin
from django.urls import path, include
from auth_api.views import UserCreateAPIView

urlpatterns = [
    path('admin/', admin.site.urls),

    # obtain and refresh token API endpoints
    path('api/v1/token/obtain/', simplejwt_views.TokenObtainPairView.as_view(), name='token_obtain'),
    path('api/v1/token/refresh/', simplejwt_views.TokenRefreshView.as_view(), name='token_refresh'),

    # User registration API endpoint
    path('api/v1/register/', UserCreateAPIView.as_view(), name='register'),

    # apps urls
    path('api/v1/', include('blog_api.urls'))
]
