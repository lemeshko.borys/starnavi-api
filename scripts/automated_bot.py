from collections import namedtuple
from random import randint
from faker import Faker

from django.db.models import Max, Min
# from django.db.utils import IntegrityError uncomment if post could not be liked multiple times
from blog_api.models import Post, Like
from auth_api.models import User
from scripts import config as _config



Config = namedtuple('Config', 'USERS_COUNT MAX_POSTS MAX_LIKES')


class AutomatedBot:

    def __init__(self, config):
        self.config = config
        self.faker = Faker()
        self.users = None
        self.posts = None

    def signup(self):
        User.objects.bulk_create((
            User(email=self.faker.email(), password=self.faker.password()) for _ in range(1, self.config.USERS_COUNT)
        ))
        self.users = User.objects.all()

    def create_posts(self):
        for user in self.users:
            # using iterator and bulk_create to speed up object creation
            Post.objects.bulk_create((Post(
                    title=self.faker.text()[:255],
                    author=user,
                    short_description=self.faker.text(),
                    full_description=self.faker.text()
                ) for _ in range(1, randint(1, self.config.MAX_POSTS))
            ))
        self.posts = Post.objects.all()

    def random_likes(self):
        max_post_id = Post.objects.aggregate(max_id=Max("id"))["max_id"]
        min_post_id = Post.objects.aggregate(min_id=Min("id"))["min_id"]
        for user in self.users:
            Like.objects.bulk_create((
                Like(post_id=randint(min_post_id, max_post_id), author=user)
                for _ in range(1, randint(1, self.config.MAX_LIKES))
            ))

            """
            in case if one post cannot be liked multiple times:
            
            for _ in range(1, randint(1, self.config.MAX_LIKES)):
                rand_post_id =randint(min_post_id, max_post_id)
                try:
                    Like.objects.create(post_id=rand_post_id, author=user)
                except IntegrityError:
                    Like.objects.filter(post_id=rand_post_id, author=user).delete()
            """


def run():
    bot = AutomatedBot(
        Config(
            _config.USERS_COUNT,
            _config.MAX_POSTS,
            _config.MAX_LIKES
        )
    )
    bot.signup()
    bot.create_posts()
    bot.random_likes()
