import requests
from django.conf import settings
from rest_framework import serializers
from rest_framework.exceptions import ValidationError
from auth_api.models import User


class UserModelSerializer(serializers.ModelSerializer):

    def validate(self, attrs):
        url = settings.EMAIL_TO_FIND_URL.format(attrs['email'])
        request = requests.get(url).json()
        if request['data']['gibberish'] or not request['data']['webmail']:
            raise ValidationError('Email is not valid or it does not exist', code=400)
        elif not attrs['first_name'] or not attrs['last_name']:
            raise ValidationError('You can`t leave this fields blank (Firs/Last Name)', code=400)
        return attrs

    class Meta:
        model = User
        fields = (
            'id',
            'email',
            'first_name',
            'last_name',
        )
