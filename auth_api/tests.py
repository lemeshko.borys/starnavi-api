from rest_framework import test, status
from auth_api.models import User


class AuthTestAPICase(test.APITestCase):

    def setUp(self) -> None:
        self.registered_user = User.objects.create_user(
            email='test@case.user',
            password='test_case_password'
        )

        self.user_data = {
            'email': 'lemeshkob@gmail.com',
            'password': 'new_user_password',
            'first_name': 'new_user_first_name',
            'last_name': 'new_user_last_name'
        }

        self.api_client = test.APIClient()

        self.register_url = '/api/v1/register/'
        self.login_url = '/api/v1/token/obtain/'

    def test_user_can_register(self):
        response = self.api_client.post(
            path=self.register_url,
            data=self.user_data
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertIsNotNone(User.objects.get(email=self.user_data['email']))
        self.assertEqual(User.objects.get(email=self.user_data['email']).email, self.user_data['email'])

    def test_user_can_authorize(self):
        response = self.api_client.post(
            path=self.login_url,
            data={
                'email': self.registered_user.email,
                'password': 'test_case_password'
            }
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertIsNotNone(response.data['access'])
        self.assertIsNotNone(response.data['refresh'])

    def test_user_cannot_register_bad_request(self):
        bad_user_data = {
            'email': True,
            'password': 344,
            'first_name': '',
            'last_name': 'new_user_last_name'
        }
        response = self.api_client.post(
            path=self.register_url,
            data=bad_user_data
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_user_cannot_login_bad_request_unauthorized(self):
        bad_login_data = {
            'email': 123,
            'password': True
        }
        response = self.api_client.post(
            path=self.login_url,
            data=bad_login_data
        )

        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_user_cannot_login_user_does_not_exist(self):
        bad_login_data = {
            'email': 'lemeshko.borys@gmail.com',
            'password': 'Qwe123qwe'
        }
        response = self.api_client.post(
            path=self.login_url,
            data=bad_login_data
        )
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

