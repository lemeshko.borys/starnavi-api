from rest_framework.generics import CreateAPIView
from auth_api.serializers import UserModelSerializer
from auth_api.models import User


class UserCreateAPIView(CreateAPIView):

    serializer_class = UserModelSerializer
    queryset = User.objects.all()
