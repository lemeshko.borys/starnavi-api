from rest_framework import status
from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView
from rest_framework.decorators import api_view, permission_classes
from rest_framework.exceptions import NotFound, MethodNotAllowed
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated, BasePermission, SAFE_METHODS
import blog_api.serializers as blog_serializers
import blog_api.models as blog_models


class PostListCreateAPIView(ListCreateAPIView):

    """
    PostListCreateAPIView class inherits from
     rest_framework.generics.ListCreateAPIView

    Provides class-based view to perform GET (list) and POST actions
     on Post model

    Uses IsAuthenticated permission class
    """

    permission_classes = [IsAuthenticated, ]
    serializer_class = blog_serializers.PostModelSerializer
    queryset = blog_models.Post.objects.all()

    def perform_create(self, serializer):
        serializer.save(author=self.request.user)


class IsPostAuthor(BasePermission):
    """
   Allow access only for object Author.
   """

    def has_object_permission(self, request, view, obj):
        if request.method in SAFE_METHODS:
            return True
        return obj.author == request.user


class PostRetrieveUpdateDestroyAPIView(RetrieveUpdateDestroyAPIView):
    """
    PostRetrieveUpdateDestroyAPIView class inherits from
     rest_framework.generics.RetrieveUpdateDestroyAPIView

    Provides class-based view to perform GET (retrieve), PUT/PATCH and DELETE actions
     on Post model

    Uses IsAuthenticated permission class
    """

    permission_classes = [IsAuthenticated, IsPostAuthor, ]
    serializer_class = blog_serializers.PostModelSerializer
    queryset = blog_models.Post.objects.all()


@api_view(['POST'])
@permission_classes([IsAuthenticated, ])
def like_post(request, pk, format=None):

    """
    like_post function-based view
    Allows to like specific Post accessing it by primary key
    Has 2 decorators: api_view(['POST]) and authentication_classes([IsAuthenticated, ])

    :param request: incoming request for this function-based view
    :param pk: primary key of the Post object that is going to be affected
    :param format: optional param. E.g. request could pass format=json
    :return: HttpResponse with serialized Post data and status code
    """

    try:
        post = blog_models.Post.objects.get(id=pk)
    except blog_models.Post.DoesNotExist:
        raise NotFound(detail="Required Post entity is not found", code=404)
    except ValueError:
        return Response(data={'error': 'Bad request.'}, status=status.HTTP_400_BAD_REQUEST)

    try:
        blog_models.Like.objects.get(post=post, author=request.user)
        return Response(data={'error': 'Multiple post likes are disallowed.'}, status=status.HTTP_400_BAD_REQUEST)

    except blog_models.Like.DoesNotExist:
        blog_models.Like.objects.create(post=post, author=request.user)
        return Response(blog_serializers.PostModelSerializer(post).data, status=status.HTTP_201_CREATED)


@api_view(['DELETE'])
@permission_classes([IsAuthenticated, ])
def unlike_post(request, pk, format=None):

    """
    unlike_post function-based view
    Allows to unlike specific Post accessing it by primary key
    Has 2 decorators: api_view(['DELETE]) and authentication_classes([IsAuthenticated, ])

    :param request: incoming request for this function-based view
    :param pk: primary key of the Post object that is going to be affected
    :param format: optional param. E.g. request could pass format=json
    :return: HttpResponse with serialized Post data and status code
    """

    try:
        post = blog_models.Post.objects.get(id=pk)
    except blog_models.Post.DoesNotExist:
        raise NotFound(detail="Required Post entity is not found", code=404)

    try:
        like = blog_models.Like.objects.get(post=post, author=request.user)
        like.delete()
        return Response(blog_serializers.PostModelSerializer(post).data, status=status.HTTP_204_NO_CONTENT)
    except blog_models.Like.DoesNotExist:
        raise NotFound(detail="Post is not liked by user.", code=404)


