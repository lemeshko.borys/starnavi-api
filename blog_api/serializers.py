from rest_framework import serializers
from auth_api.serializers import UserModelSerializer
import blog_api.models as blog_models


class PostModelSerializer(serializers.ModelSerializer):

    """
    PostModelSerializer class

    Class-serializer that provides serialization API to Post model

    Serialized fields:
    id, title, author, likes, short_description, full_description, created, last_updated_at
    """

    author = UserModelSerializer(read_only=True)

    class Meta:
        model = blog_models.Post
        fields = (
            'id',
            'title',
            'author',
            'likes',
            'short_description',
            'full_description',
            'created',
            'last_updated_at'
        )
