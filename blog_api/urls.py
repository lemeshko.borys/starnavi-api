from django.urls import path
import blog_api.views as blog_views

urlpatterns = [
    # path to list or create new Post
    path('posts/', blog_views.PostListCreateAPIView.as_view(), name='posts-list-create'),

    # path to retrieve update or destroy one specific Post
    path('posts/<int:pk>/', blog_views.PostRetrieveUpdateDestroyAPIView.as_view(), name='post-rud'),

    # paths to like/unlike specific Post
    path('posts/<int:pk>/like/', blog_views.like_post, name='like-post'),
    path('posts/<int:pk>/unlike/', blog_views.unlike_post, name='unlike-post')
]
