from rest_framework import test, status
from blog_api.models import Post, Like
from auth_api.models import User


class UserCanUnlikePostsAPITestCase(test.APITestCase):

    def setUp(self) -> None:
        self.user = User.objects.create_user(
            email='test@case.user',
            password='test_case_user_password'
        )

        self.post = Post.objects.create(
            title='title',
            author=self.user,
            short_description='short',
            full_description='full'
        )

        self.api_client = test.APIClient()
        auth_data = self.api_client.post(
            path='/api/v1/token/obtain/',
            data={
                'email': self.user.email,
                'password': 'test_case_user_password'
            }
        )
        self.api_client.credentials(HTTP_AUTHORIZATION='Bearer ' + auth_data.data['access'])
        self.url = f'/api/v1/posts/{self.post.id}/unlike/'
        self.like = Like.objects.create(post=self.post, author=self.user)

    def test_user_can_unlike_post(self):
        response = self.api_client.delete(self.url)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_user_cannot_unlike_post_was_not_liked(self):
        self.like.delete()
        response = self.api_client.delete(self.url)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_user_cannot_unlike_unauthorized(self):
        self.api_client.logout()
        response = self.api_client.delete(self.url)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

