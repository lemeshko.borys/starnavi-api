from rest_framework import test, status
from blog_api.models import Post, Like
from auth_api.models import User


class UserCanLikePostsAPITestCase(test.APITestCase):

    def setUp(self) -> None:
        self.user = User.objects.create_user(
            email='test@case.user',
            password='test_case_user_password'
        )

        self.post = Post.objects.create(
            title='title',
            author=self.user,
            short_description='short',
            full_description='full'
        )

        self.api_client = test.APIClient()
        auth_data = self.api_client.post(
            path='/api/v1/token/obtain/',
            data={
                'email': self.user.email,
                'password': 'test_case_user_password'
            }
        )
        self.api_client.credentials(HTTP_AUTHORIZATION='Bearer ' + auth_data.data['access'])
        self.url = f'/api/v1/posts/{self.post.id}/like/'

    def test_user_can_like_post(self):
        response = self.api_client.post(self.url)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data['likes'], self.post.likes)

    def test_user_cannot_like_already_liked(self):
        Like.objects.create(post=self.post, author=self.user)
        response = self.api_client.post(self.url)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_user_cannot_like_unauthorized(self):
        self.api_client.logout()
        response = self.api_client.post(self.url)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

