from rest_framework import test, status
from blog_api.models import Post
from auth_api.models import User


class UserCanDeletePostAPITestCase(test.APITestCase):

    def setUp(self) -> None:
        self.user = User.objects.create_user(
            email='test@case.email',
            password='test_case_password'
        )

        self.post = Post.objects.create(
            author=self.user,
            short_description='short',
            full_description='full'
        )

        self.api_client = test.APIClient()
        auth_data = self.api_client.post(
            path='/api/v1/token/obtain/',
            data={
                'email': self.user.email,
                'password': 'test_case_password'
            }
        )
        self.api_client.credentials(HTTP_AUTHORIZATION='Bearer ' + auth_data.data['access'])

        self.url = f'/api/v1/posts/{self.post.id}/'

    def test_user_can_delete_post(self):
        response = self.api_client.delete(path=self.url)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertIsNone(Post.objects.first())

    def test_user_cannot_delete_post_not_authorized(self):
        self.api_client.logout()
        response = self.api_client.delete(path=self.url)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.assertIsNotNone(Post.objects.first())

    def test_user_cannot_delete_post_permission_denied(self):
        self.api_client.logout()
        bad_user = User.objects.create_user(
            email='bad@user.email',
            password='bad_user_password'
        )
        auth_data = self.api_client.post(
            path='/api/v1/token/obtain/',
            data={
                'email': bad_user.email,
                'password': 'bad_user_password'
            }
        )
        self.api_client.credentials(HTTP_AUTHORIZATION='Bearer ' + auth_data.data['access'])

        response = self.api_client.delete(path=self.url)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertIsNotNone(Post.objects.first())
