from rest_framework import test, status
from auth_api.models import User
from blog_api.models import Post


class UserCanCreatePostAPITestCase(test.APITestCase):

    def setUp(self) -> None:
        self.user = User.objects.create_user(
            email='test@case.email',
            password='test_case_password'
        )

        self.api_client = test.APIClient()
        auth_data = self.api_client.post(
            path='/api/v1/token/obtain/',
            data={
                'email': self.user.email,
                'password': 'test_case_password'
            }
        )

        self.post_data = {
            'title': 'post_title',
            'short_description': 'post_short_description',
            'full_description': 'post_full_description'
        }

        self.api_client.credentials(HTTP_AUTHORIZATION='Bearer ' + auth_data.data['access'])
        self.url = '/api/v1/posts/'

    def test_user_can_create_post(self):
        response = self.api_client.post(
            path=self.url,
            data=self.post_data
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data['title'], Post.objects.first().title)

    def test_user_cannot_create_post_not_authorized(self):
        self.api_client.logout()
        response = self.api_client.post(
            path=self.url,
            data=self.post_data
        )
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_user_cannot_create_post_bad_request(self):

        post_bad_data = {
            'title': 123,
            'full_description': 'post_full_description',
            'hello': True
        }

        response = self.api_client.post(
            path=self.url,
            data=post_bad_data
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
