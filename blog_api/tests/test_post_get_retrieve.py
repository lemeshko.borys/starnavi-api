from rest_framework import test, status
from rest_framework.request import Request
from blog_api.models import Post
from blog_api.serializers import PostModelSerializer
from auth_api.models import User

FACTORY = test.APIRequestFactory()


class UserCanGetRetrievePostsAPITestCase(test.APITestCase):

    def setUp(self) -> None:
        self.user = User.objects.create_user(
            email='test@case.user',
            password='test_case_user_password'
        )
        self.post = Post.objects.create(
            title='title',
            author=self.user,
            short_description='short',
            full_description='full'
        )

        for _ in range(1, 5):
            Post.objects.create(
                title='title' + str(_),
                author=self.user,
                short_description='short' + str(_),
                full_description='full' + str(_)
            )
        self.api_client = test.APIClient()
        auth_data = self.api_client.post(
            path='/api/v1/token/obtain/',
            data={
                'email': self.user.email,
                'password': 'test_case_user_password'
            }
        )
        self.api_client.credentials(HTTP_AUTHORIZATION='Bearer ' + auth_data.data['access'])
        self.get_url = '/api/v1/posts/'
        self.retrieve_url = f'/api/v1/posts/{self.post.id}/'
        self.get_request = Request(FACTORY.get(self.get_url))

    def test_user_can_get_posts(self):
        response = self.api_client.get(self.get_url)
        self.assertEqual(
            response.data,
            PostModelSerializer(
                self.user.post_set.order_by('id'),
                many=True,
                context={'request': self.get_request}
            ).data
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_user_can_retrieve_post(self):
        response = self.api_client.get(self.retrieve_url)
        self.assertEqual(
            response.data,
            PostModelSerializer(self.post).data
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_user_cannot_get_posts_unauthorized(self):
        self.api_client.logout()
        response = self.api_client.get(self.get_url)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_user_cannot_retrieve_posts_unauthorized(self):
        self.api_client.logout()
        response = self.api_client.get(self.retrieve_url)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_user_cannot_retrieve_posts_not_found(self):
        response = self.api_client.get('/api/v1/posts/342/')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
