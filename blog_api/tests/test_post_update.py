from rest_framework import test, status
from auth_api.models import User
from blog_api.models import Post
import datetime


class UserCanUpdatePostAPITestCase(test.APITestCase):

    def setUp(self) -> None:

        self.user = User.objects.create_user(
            email='test@case.email',
            password='test_case_password'
        )

        self.api_client = test.APIClient()
        auth_data = self.api_client.post(
            path='/api/v1/token/obtain/',
            data={
                'email': self.user.email,
                'password': 'test_case_password'
            }
        )
        self.api_client.credentials(HTTP_AUTHORIZATION='Bearer ' + auth_data.data['access'])

        self.post = Post.objects.create(
            title='title',
            author=self.user,
            short_description='short_description',
            full_description='full_description'
        )

        self.put_data = {
            'title': 'new_title',
            'short_description': 'new_short_description',
            'full_description': 'new_full_description'
        }

        self.url = f'/api/v1/posts/{self.post.id}/'

    def test_user_can_update_post(self):
        response = self.api_client.put(
            path=self.url,
            data=self.put_data
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['title'], self.put_data['title'])
        self.assertTrue(response.data['created'] < response.data['last_updated_at'])

    def test_user_cannot_update_not_authorized(self):
        self.api_client.logout()
        response = self.api_client.put(
            path=self.url,
            data=self.put_data
        )
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_user_cannot_update_permission_denied(self):
        self.api_client.logout()
        new_user = User.objects.create_user(email='new@user.email', password='new_password')
        auth_data = self.api_client.post(
            path='/api/v1/token/obtain/',
            data={
                'email': new_user.email,
                'password': 'new_password'
            }
        )
        self.api_client.credentials(HTTP_AUTHORIZATION='Bearer ' + auth_data.data['access'])

        response = self.api_client.put(
            path=self.url,
            data=self.put_data
        )

        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_user_cannot_update_bad_request(self):
        bad_put_data = {
            'title': True,
            'short_description': datetime.datetime.now()
        }
        response = self.api_client.put(
            path=self.url,
            data=bad_put_data
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
