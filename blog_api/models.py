from django.db import models


class Like(models.Model):

    """
    Like class

    Represents 'likes' database table. Adjacent model (table) that allows system
    to store information regarding users' likes

    Contains:
    post - foreign_key to blog_api.Post entity
    author - foreign_key to django.contrib.auth.models.User entity
    """

    post = models.ForeignKey(
        'blog_api.Post',
        on_delete=models.CASCADE
    )

    author = models.ForeignKey(
        'auth_api.User',
        on_delete=models.CASCADE,
        null=True
    )

    def __str__(self):
        return f'{self.post} - {self.author}'

    class Meta:
        db_table = 'likes'


class Post(models.Model):

    """
    Post class

    Represents 'posts' database table. Stores data regarding specific user`s blog post

    Contains:
    title - post title - CharField max_length 255
    author - foreign_key to django.contrib.auth.models.User entity
    short_description - contains short post description for previews. TextField()
    full_description - contains full post description for long read. TextField()
    created - contains DateTime information when post was created. DateTimeField()
    last_updated_at - contains DateTime information when post was last updated. DateTimeField()

    likes - @property - returns the value of post likes
    """

    title = models.CharField(max_length=255)

    author = models.ForeignKey(
        'auth_api.User',
        on_delete=models.CASCADE
    )

    short_description = models.TextField()
    full_description = models.TextField()
    created = models.DateTimeField(auto_now_add=True)
    last_updated_at = models.DateTimeField(auto_now=True)

    @property
    def likes(self):
        return self.like_set.count()

    def __str__(self):
        return self.title

    class Meta:
        db_table = 'posts'
